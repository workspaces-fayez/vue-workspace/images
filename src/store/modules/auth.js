import qs from 'qs';
import api from '../../api/imgur';
import { router } from '../../main';

const state = {
    token: window.localStorage.getItem('imgur_token')
};

const getters = {
    isLoggedIn: (state) => !!state.token
};

const actions = {
    login: () => {
        api.login();
    },
    finalizeLogin: ({ commit }, hash) => {
        const query = qs.parse(hash.replace('#', ''));
        commit('SET_TOKEN', query.access_token);
        window.localStorage.setItem('imgur_token', query.access_token);
        router.push('/');
    },
    logout: ({ commit }) => {
        commit('SET_TOKEN', null);
        window.localStorage.removeItem('imgur_token');
    },
};

const mutations = {
    SET_TOKEN: (state, value) => {
        state.token = value;
    }
};

export default {
    state,
    getters,
    actions,
    mutations
};