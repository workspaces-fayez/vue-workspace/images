import api from '../../api/imgur';
import { router } from '../../main';

const state = {
	images: []
};

const getters = {
	allImages: () => state.images
};

const actions = {
	fetchImages: async ({ rootState, commit }) => {
		const { token } = rootState.auth;
		const response = await api.fetchImages(token);
		commit('SET_IMAGES', response.data.data);
	},
	uploadImages: async ({ rootState }, images) => {
		const { token } = rootState.auth;
		await api.upload(images, token);
		router.push('/');
	}
};

const mutations = {
	SET_IMAGES: (state, value) => {
		state.images = value;
	}
};

export default {
	state,
	getters,
	actions,
	mutations
};
